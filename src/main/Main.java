package main;

import java.util.List;

import javax.security.auth.login.LoginException;

import music.MusicManager;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.MessageReaction.ReactionEmote;
import net.dv8tion.jda.core.entities.VoiceChannel;
import net.dv8tion.jda.core.events.Event;
import net.dv8tion.jda.core.events.guild.voice.GuildVoiceLeaveEvent;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.hooks.EventListener;
import net.dv8tion.jda.core.managers.AudioManager;
import request.Request;
import request.RequestBuilder;
import request.RequestFailedException;
import vote.VoteMessageListener;

public class Main {
	
	/**
	 * Classe interne qui déclence les evenements liés aux messages
	 * @author hugo
	 *
	 */
	public static class MessageListener implements EventListener {

		@Override
		public void onEvent(Event event) {
			if(event instanceof GuildMessageReceivedEvent) {
				GuildMessageReceivedEvent messageEvent = (GuildMessageReceivedEvent) event;
				if(messageEvent.getMessage().getContentDisplay().startsWith("-zik ")) {
					try {
						Request requete = RequestBuilder.getRequestOf(messageEvent.getMessage());
						requete.execute();
					}catch(RequestFailedException e) {
						messageEvent.getMessage().getTextChannel().sendMessage("ERREUR : " + e.getMessage() + " :/").queue();
					}
				}
			}
		}
	}
	
	/**
	 * Classe interne qui gère les evènements lorsque quelqu'un quitte
	 * @author hugo
	 *
	 */
	public static class LeavingListener implements EventListener {

		@Override
		public void onEvent(Event event) {
			if(event instanceof GuildVoiceLeaveEvent) {
				GuildVoiceLeaveEvent leaveEvent = (GuildVoiceLeaveEvent) event;
				VoiceChannel channel = leaveEvent.getChannelLeft();
				AudioManager manager = leaveEvent.getGuild().getAudioManager();
				if(channel.getMembers().size() == 1 && channel.equals(manager.getConnectedChannel())) {
					MusicManager.forceQuit(leaveEvent.getGuild());
				}
			}
		}
	}

	public static void main(String[] args) {
		JDABuilder jda = new JDABuilder(args[0]);
		jda.addEventListener(new MessageListener(), new LeavingListener(), new VoteMessageListener());
		jda.setGame(Game.listening("-zik help"));
		try {
			JDA jdaBuilded = jda.build();
			VoteMessageListener.yesEmote = new ReactionEmote("👍", null, jdaBuilded);
			VoteMessageListener.noEmote = new ReactionEmote("👎", null, jdaBuilded);
		} catch (LoginException e) {
			e.printStackTrace();
		}
	}

}
