package vote;

import request.RequestFailedException;

/**
 * Interface représentant une action a effectuer aprés le résultat d'un bote
 * @author hugo
 *
 */
public interface VoteReaction {
	
	/**
	 * Fonction a effectuer après le vote
	 * @param result Le résultat du vote
	 */
	public void onEndOfVote(boolean result) throws RequestFailedException;

}
