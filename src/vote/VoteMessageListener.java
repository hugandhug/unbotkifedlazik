package vote;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import music.MusicManager;
import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.entities.Emote;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageReaction;
import net.dv8tion.jda.core.entities.VoiceChannel;
import net.dv8tion.jda.core.entities.MessageReaction.ReactionEmote;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.Event;
import net.dv8tion.jda.core.events.message.MessageUpdateEvent;
import net.dv8tion.jda.core.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.core.hooks.EventListener;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import request.RequestFailedException;

/**
 * Classe qui permet de réaliser des votes avec des emotes
 * @author hugo
 *
 */
public class VoteMessageListener implements EventListener {
	
	private static Map<Long, VoteReaction> messageIds = new HashMap<Long, VoteReaction>();
	public static ReactionEmote yesEmote;
	public static ReactionEmote noEmote;
	
	@Override
	public void onEvent(Event event) {
		if(event instanceof MessageReactionAddEvent) {
			MessageReactionAddEvent messageEvent = (MessageReactionAddEvent) event;
			if(messageIds.containsKey(messageEvent.getMessageIdLong())) {
				Message msg = messageEvent.getTextChannel().getMessageById(messageEvent.getMessageId()).complete();
				VoiceChannel voiceChannel = messageEvent.getGuild().getAudioManager().getConnectedChannel();
				int nbYes = countYes(msg, voiceChannel);
				int nbNo = countNo(msg, voiceChannel);
				Set<User> users = new HashSet<User>();
				for(Member member : voiceChannel.getMembers()) {
					User usr = member.getUser();
					if(!usr.isBot()) users.add(usr);
				}
				try {
					if(nbYes > (users.size() / 2)) {
						messageIds.get(msg.getIdLong()).onEndOfVote(true);
						messageIds.remove(msg.getIdLong());
					}
					else if(nbNo > (users.size() / 2)) {
						messageIds.get(msg.getIdLong()).onEndOfVote(false);
						messageIds.remove(msg.getIdLong());
					}
				}catch(RequestFailedException e) { }
			}
		}
	}
	
	/**
	 * Compte le nombre de oui dans un message
	 * @param message Le message
	 * @param channel Le channel vocal comportant les voteurs
	 * @return Le nombre de oui
	 */
	private static int countYes(Message message, VoiceChannel channel) {
		return countEmote(message, yesEmote, channel);
	}
	
	/**
	 * Compte le nombre de non dans un message
	 * @param message Le message
	 * @param channel Le channel vocal comportant les voteurs
	 * @return Le nombre de non
	 */
	private static int countNo(Message message, VoiceChannel channel) {
		return countEmote(message, noEmote, channel);
	}
	
	/**
	 * Compte le nombre de personnes qui ont séléctionné une emote dans un message
	 * @param message Le message
	 * @param emote L'emote
	 * @param channel Le channel vocal comportant les voteurs
	 * @return Le nombre de personne qui ont séléctionné l'émote
	 */
	private static int countEmote(Message message, ReactionEmote emote, VoiceChannel channel) {
		MessageReaction reaction;
		Iterator<MessageReaction> reactions = message.getReactions().iterator();
		try {
			do {
				reaction = reactions.next();
			}while(!reaction.getReactionEmote().equals(emote));
		}catch(NoSuchElementException e) {
			return 0;
		}
		Set<User> usersOfChannel = new HashSet<User>();
		for(Member member : channel.getMembers()) {
			User user = member.getUser();
			if(!user.isBot()) usersOfChannel.add(member.getUser());
		}
		List<User> res = reaction.getUsers().complete();
		res.retainAll(usersOfChannel);
		return res.size();
	}
	
	/**
	 * Ajoute un message dans la liste des messages a surveiller
	 * @param message
	 */
	public static void addMessage(Message message, VoteReaction reaction) {
		messageIds.put(message.getIdLong(), reaction);
	}

}
