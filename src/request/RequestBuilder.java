package request;

import net.dv8tion.jda.core.entities.Message;

/**
 * Builder pour retourner la requête correspondant a un message
 * @author hugo
 *
 */
public class RequestBuilder {
	/**
	 * Retourne une requête associé a un message
	 * @param msg Le message
	 * @return Une requête associé au message
	 * @throws IllegalArgumentException Si le message contient des arguments incorrects
	 */
	public static Request getRequestOf(Message msg) throws RequestFailedException{
		String arg2[] = msg.getContentDisplay().toUpperCase().replaceAll(" +", " ").split(" ");
		switch(arg2[1]) {
		case "JOIN":
			return new JoinRequest(msg);
		case "ADD":
			return new AddRequest(msg);
		case "HELP":
			return new HelpRequest(msg);
		case "SKIP":
			return new SkipRequest(msg);
		case "REMOVE":
			return new RemoveRequest(msg);
		case "EXIT":
			return new ExitRequest(msg);
		default:
			return new InfoRequest(msg);
		}
	}
}
