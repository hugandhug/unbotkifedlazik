package request;

import net.dv8tion.jda.core.entities.GuildVoiceState;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.managers.AudioManager;

public class ExitRequest extends Request {

	private AudioManager AUDIO_MANAGER;

	public ExitRequest(Message msg) {
		super(msg);
		this.AUDIO_MANAGER = msg.getGuild().getAudioManager();		
	}

	@Override
	public void execute() throws RequestFailedException {
		AUDIO_MANAGER.closeAudioConnection();
	}

}
