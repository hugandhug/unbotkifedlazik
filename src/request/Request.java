package request;

import java.util.Arrays;

import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;

/**
 * Classe abstraite représentant une requête utilisateur
 * @author mathis
 *
 */
public abstract class Request {
	
	public final Member MEMBER;
	public final String[] ARGS;
	public final TextChannel TEXT_CHANNEL;
	
	public Request(Member member, String msg, TextChannel channel) {
		this(member, msg.replaceAll(" +", " ").split(" "), channel);
	}
	
	public Request(Message msg) {
		this(msg.getMember(), msg.getContentDisplay(), (TextChannel) msg.getChannel());
	}

	public Request(Member mEMBER, String[] aRGS, TextChannel channel) {
		MEMBER = mEMBER;
		ARGS = aRGS;
		for(int i = 0; i < ARGS.length; i++) {
			ARGS[i] = ARGS[i];
		}
		TEXT_CHANNEL = channel;
	}

	@Override
	public String toString() {
		return "Request [MEMBER=" + MEMBER + ", ARGS=" + Arrays.toString(ARGS) + "]";
	}
	
	/**
	 * Fonction qui éxécute le traitement d'une requête
	 * @throws RequestFailedException 
	 */
	public abstract void execute() throws RequestFailedException;

}
