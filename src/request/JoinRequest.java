package request;

import music.MusicManager;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.VoiceChannel;
import net.dv8tion.jda.core.managers.AudioManager;

/**
 * Classe représentant une requête pour que le bot rejoigne un channel
 * @author hugo
 *
 */
public class JoinRequest extends Request {
	
	public final VoiceChannel VOICE_CHANNEL;

	public JoinRequest(Message msg) {
		super(msg);
		this.VOICE_CHANNEL = msg.getMember().getVoiceState().getChannel();
	}

	public JoinRequest(Member member, String msg, TextChannel tChannel, VoiceChannel cHANNEL) {
		super(member, msg, tChannel);
		VOICE_CHANNEL = cHANNEL;
	}

	public JoinRequest(Member mEMBER, String[] aRGS, TextChannel tChannel, VoiceChannel cHANNEL) {
		super(mEMBER, aRGS, tChannel);
		VOICE_CHANNEL = cHANNEL;
	}

	@Override
	public String toString() {
		return "JoinRequest [CHANNEL=" + VOICE_CHANNEL + ", toString()=" + super.toString() + "]";
	}

	@Override
	public void execute() throws RequestFailedException{
		try {
			AudioManager manager = VOICE_CHANNEL.getGuild().getAudioManager();
			manager.openAudioConnection(VOICE_CHANNEL);
		}catch (IllegalArgumentException | NullPointerException e) {
			throw new RequestFailedException("Vous êtes connecté a aucun channel");
		}
		new MessageBuilder("DJ Zik est dans la place!").sendTo(TEXT_CHANNEL).queue();
	}

}
