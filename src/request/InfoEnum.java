package request;

/**
 * Enumération représentant les differents informations qu'on peut demander au bot par rapport a la musique
 * @author hugo
 *
 */
public enum InfoEnum {
	TRACK,
	POSITION,
	QUEUE;
}
