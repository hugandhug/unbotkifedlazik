package request;

import music.MusicManager;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;

/**
 * Requête d'ajout d'une musique
 * @author hugo
 *
 */
public class AddRequest extends Request {
	
	public final String keyWord;
	public final boolean isSearched;

	public AddRequest(Member member, String msg, TextChannel channel) throws RequestFailedException {
		this(member, msg.split(" +"), channel);
	}

	public AddRequest(Member mEMBER, String[] aRGS, TextChannel channel) throws RequestFailedException {
		super(mEMBER, aRGS, channel);
		try {
			if(ARGS[2].toUpperCase().equals("SEARCH")) {
				isSearched = true;
				StringBuilder sb;
				try {
					sb = new StringBuilder(ARGS[3]);
				}catch(ArrayIndexOutOfBoundsException e) {
					throw new RequestFailedException("Vous avez oublié de mettre des mots clés");
				}
				for(int i = 4; i < ARGS.length; i++) {
					sb.append(" ").append(ARGS[i]);
				}
				this.keyWord = sb.toString();
			}else {
				isSearched = false;
				this.keyWord = ARGS[2];
			}
		}catch(ArrayIndexOutOfBoundsException e) {
			throw new RequestFailedException("Vous avez oublié le lien ou bien search suivi des mots clés");
		}
	}

	public AddRequest(Message msg) throws RequestFailedException {
		this(msg.getMember(), msg.getContentDisplay(), msg.getTextChannel());
	}

	@Override
	public void execute() {
		MusicManager.addRequest(this);
	}

}
