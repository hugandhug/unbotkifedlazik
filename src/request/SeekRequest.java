package request;

import com.sedmelluq.discord.lavaplayer.track.AudioTrack;

import music.MusicManager;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;

public class SeekRequest extends Request {
	
	public final int nbSeconds;

	public SeekRequest(Member member, String msg, TextChannel channel) throws RequestFailedException {
		this(member, msg.split(" +"), channel);
	}

	public SeekRequest(Member mEMBER, String[] aRGS, TextChannel channel) throws RequestFailedException {
		super(mEMBER, aRGS, channel);
		try {
			nbSeconds = Integer.parseInt(ARGS[2]);
		}catch(ArrayIndexOutOfBoundsException e) {
			throw new RequestFailedException("Vous avez oublié de préciser le nombre de secondes a passer");
		}catch(NumberFormatException e) {
			throw new RequestFailedException(ARGS[2] + " est pas un nombre!");
		}
	}

	public SeekRequest(Message msg) throws RequestFailedException {
		this(msg.getMember(), msg.getContentDisplay(), msg.getTextChannel());
	}

	@Override
	public void execute() throws RequestFailedException {
		AudioTrack track;
		try {
			track = MusicManager.getAudioPlayer(MEMBER.getGuild()).getPlayingTrack();
		}catch(NullPointerException e) {
			throw new RequestFailedException("Aucune musique est jouée pour le moment");
		}
		track.setPosition(track.getPosition() + nbSeconds * 1000);
	}

}
