package request;

import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;
import vote.VoteMessageListener;
import vote.VoteReaction;

/**
 * Classe représentant une requête pour faire un vote
 * @author hugo
 *
 */
public abstract class VoteRequest extends Request implements VoteReaction{

	public VoteRequest(Member member, String msg, TextChannel channel) {
		super(member, msg, channel);
	}

	public VoteRequest(Member mEMBER, String[] aRGS, TextChannel channel) {
		super(mEMBER, aRGS, channel);
	}

	public VoteRequest(Message msg) {
		super(msg);
	}
	
	/**
	 * Retourne le message a afficher lors du vote
	 * @return Le message a afficher lors du vote
	 */
	public abstract String getMessageOfVote();
	
	/**
	 * Indique si l'action peut être faite directement sans vote
	 * @return Si l'action peut être faite directement sans vote
	 */
	public abstract boolean canBeDoneDirectly();

	@Override
	public void execute() throws RequestFailedException {
		try {
			if(canBeDoneDirectly()) onEndOfVote(true);
			else {
				new MessageBuilder(getMessageOfVote()).sendTo(TEXT_CHANNEL).queue(msg -> {
					msg.addReaction(VoteMessageListener.yesEmote.getName()).queue();
					msg.addReaction(VoteMessageListener.noEmote.getName()).queue();
					VoteMessageListener.addMessage(msg, this);
				});
			}
		}catch(NullPointerException e) {
			throw new RequestFailedException("Aucune musique est joué actuellement");
		}
	}
}
