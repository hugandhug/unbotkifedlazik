package request;

import music.MusicManager;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;

/**
 * Classe représentant une requête pour vouloir passer une musique
 * @author hugo
 *
 */
public class SkipRequest extends VoteRequest {

	public SkipRequest(Member member, String msg, TextChannel channel) {
		super(member, msg, channel);
	}

	public SkipRequest(Member mEMBER, String[] aRGS, TextChannel channel) {
		super(mEMBER, aRGS, channel);
	}

	public SkipRequest(Message msg) {
		super(msg);
	}

	@Override
	public void onEndOfVote(boolean result) throws RequestFailedException {
		if(result) {
			try {
				MusicManager.skipMusic(MEMBER.getGuild());
				TEXT_CHANNEL.sendMessage("La musique actuel a été skip!").queue();
			}catch(NullPointerException e) {
				throw new RequestFailedException("Aucune musique est joué en ce moment, la musique ne peut être skip");
			}
		}
	}

	@Override
	public String getMessageOfVote() {
		return "Voulez-vous passer cette musique?";
	}

	@Override
	public boolean canBeDoneDirectly() {
		return MEMBER.equals(MusicManager.getActualMember(MEMBER.getGuild()));
	}

}
