package request;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Queue;

import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo;

import music.MusicManager;
import music.MusicManager.QueueEntry;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;

/**
 * Classe représentant une requête de controle
 * @author hugo
 *
 */
public class InfoRequest extends Request {

	public final InfoEnum ENUM;
	
	public InfoRequest(Message msg) throws RequestFailedException{
		this(msg.getMember(), msg.getContentDisplay(), msg.getTextChannel());
	}

	public InfoRequest(Member member, String msg, TextChannel channel) throws RequestFailedException{
		this(member, msg.split(" +"), channel);
	}

	public InfoRequest(Member member, String[] msg, TextChannel channel) throws RequestFailedException{
		super(member, msg, channel);
		try {
			this.ENUM = InfoEnum.valueOf(ARGS[1].toUpperCase());
		}catch(ArrayIndexOutOfBoundsException e) {
			throw new RequestFailedException("Vous avez oublié de mettre une commande");
		}catch(EnumConstantNotPresentException e) {
			throw new RequestFailedException("Je ne comprends pas " + ARGS[1]);
		}
	}

	@Override
	public String toString() {
		return "ControlRequest [ENUM=" + ENUM + ", toString()=" + super.toString() + "]";
	}

	@Override
	public void execute() throws RequestFailedException {
		String text;
		AudioTrack track;
		try {
			track = MusicManager.getAudioPlayer(MEMBER.getGuild()).getPlayingTrack();
		}catch(NullPointerException e) {
			throw new RequestFailedException("Aucune musique est joué en ce moment");
		}
		switch(ENUM) {
		case POSITION:
			int timeNowSecond = (int) (track.getPosition() / 1000);
			int durationSecond = (int) (track.getDuration() / 1000);
			LocalTime timeNow = LocalTime.of((timeNowSecond / 3600) % 3600, (timeNowSecond / 60) % 60, timeNowSecond % 60);
			LocalTime timeDuration = LocalTime.of((durationSecond / 3600) % 3600, (durationSecond / 60) % 60, durationSecond % 60);
			text = "Position de " + track.getInfo().title + " : " + timeNow.getMinute() + "m" + timeNow.getSecond() + "s / " + timeDuration.getMinute() + "m" + timeDuration.getSecond() + "s";
			break;
		case TRACK:
			AudioTrackInfo info = track.getInfo();
			text = "Musique actuel : \nTitre : " + info.title + "\nAuteur : " + info.author + "\nLien : " + info.uri;
			break;
		case QUEUE:
			Queue<Member> queue = MusicManager.getQueue(MEMBER.getGuild());
			if(queue.isEmpty()) text = "La file d'attente est vide";
			else {
				StringBuilder builder = new StringBuilder("File d'attente pour ce serveur : \n");
				for(QueueEntry trackInQueue : MusicManager.getAudioTrackQueue(MEMBER.getGuild())) {
					builder.append("De " + trackInQueue.member.getEffectiveName() + " : " + trackInQueue.track.getInfo().title + '\n');
				}
				text = builder.toString();
			}
			break;
		default:
			text = "Commande incomprise";
		}
		new MessageBuilder(text).sendTo(TEXT_CHANNEL).queue();
	}
}
