package request;

public class RequestFailedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RequestFailedException(String arg0) {
		super(arg0);
	}

}
