package request;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.entities.TextChannel;

/**
 * Requête pour connaitre les commandes du bot
 * @author hugo
 *
 */
public class HelpRequest extends Request{
	
	private final static MessageEmbed embded;
	
	static {
		Map<String, String> commands = new LinkedHashMap<String, String>();
		commands.put("join", "Rejoindre le channel dans lequel vous êtes");
		commands.put("add [Lien Youtube/Sound Cloud/Band Camp/Vimeo/Twitch OU Lien direct]", "Ajouter une musique a la file d'attente");
		commands.put("add search [Mot clés]", "Ajoute le résultat d'une recherche youtube");
		commands.put("remove [Numero dans la file d'attente ou nom du titre]", "Enlever une musique");
		commands.put("skip", "Passer la musique joué");
		commands.put("seek [Nombre de secondes]", "Passer la musique d'un certain nombre de secondes");
		commands.put("track", "Affiche des informations sur la musique joué");
		commands.put("queue", "Affiche la file d'attente des musiques");
		commands.put("position", "Affiche la position actuel de la musique");
		commands.put("exit", "Quitte la guilde actuelle");
		
		EmbedBuilder builder = new EmbedBuilder();
		builder.setAuthor("Zik vous cause");
		builder.setTitle("Les commandes de zik");
		builder.setDescription("Syntaxe : -zik COMMANDE");
		for(Entry<String, String> entry : commands.entrySet()) {
			builder.addField(entry.getKey(), entry.getValue(), false);
		}
		embded = builder.build();
	}

	public HelpRequest(Member member, String msg, TextChannel channel) {
		super(member, msg, channel);
	}

	public HelpRequest(Member mEMBER, String[] aRGS, TextChannel channel) {
		super(mEMBER, aRGS, channel);
	}

	public HelpRequest(Message msg) {
		super(msg);
	}

	@Override
	public void execute() {
		TEXT_CHANNEL.sendMessage(embded).queue();
	}

}
