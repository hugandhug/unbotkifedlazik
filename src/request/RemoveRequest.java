package request;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import com.sedmelluq.discord.lavaplayer.track.AudioTrack;

import music.MusicManager;
import music.MusicManager.QueueEntry;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;

/**
 * Classe représentant une requête pour enlever une musique
 * @author hugo
 *
 */
public class RemoveRequest extends VoteRequest {
	
	public final QueueEntry TO_REMOVE;

	public RemoveRequest(Member member, String msg, TextChannel channel) throws RequestFailedException {
		this(member, msg.split(" +"), channel);
	}

	public RemoveRequest(Member mEMBER, String[] aRGS, TextChannel channel) throws RequestFailedException {
		super(mEMBER, aRGS, channel);
		QueueEntry toRemove;
		try {
			int nbToRemove = Integer.parseInt(ARGS[2]);
			List<QueueEntry> toAdd = new ArrayList<MusicManager.QueueEntry>(MusicManager.getAudioTrackQueue(MEMBER.getGuild()));
			toRemove = toAdd.get(nbToRemove);
		}catch(NumberFormatException e) {
			Iterator<QueueEntry> entries = MusicManager.getAudioTrackQueue(MEMBER.getGuild()).iterator();
			try {
				do {
					toRemove = entries.next();
				}while(toRemove.track.getInfo().title.toUpperCase().contains(ARGS[2].toUpperCase()));
			}catch(NoSuchElementException ee) {
				throw new RequestFailedException("La musique " + ARGS[2] + " n'as pas pu être trouvé");
			}
		}catch(ArrayIndexOutOfBoundsException e) {
			throw new RequestFailedException("Il manque le numero de la piste ou bien son nom");
		}
		TO_REMOVE = toRemove;
	}

	public RemoveRequest(Message msg) throws RequestFailedException {
		this(msg.getMember(), msg.getContentDisplay(), msg.getTextChannel());
	}

	@Override
	public void onEndOfVote(boolean result) {
		if(result) {
			MusicManager.getMemberMap(MEMBER.getGuild()).get(TO_REMOVE.member).remove(TO_REMOVE.track);
			TEXT_CHANNEL.sendMessage("La musique actuel a été skip");
		}
	}

	@Override
	public String getMessageOfVote() {
		return "Voulez vous enlever la musique " + TO_REMOVE.track.getInfo().title + " de " + TO_REMOVE.member.getEffectiveName() + "?";
	}

	@Override
	public boolean canBeDoneDirectly() {
		return TO_REMOVE.member.equals(MEMBER);
	}

}
