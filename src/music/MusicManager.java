package music;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Map.Entry;

import com.sedmelluq.discord.lavaplayer.player.AudioConfiguration;
import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.format.AudioDataFormat;
import com.sedmelluq.discord.lavaplayer.player.AudioConfiguration.ResamplingQuality;
import com.sedmelluq.discord.lavaplayer.player.event.AudioEvent;
import com.sedmelluq.discord.lavaplayer.player.event.AudioEventAdapter;
import com.sedmelluq.discord.lavaplayer.player.event.AudioEventListener;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManager;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioItem;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioReference;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo;
import com.sedmelluq.discord.lavaplayer.track.playback.AudioFrame;

import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.audio.AudioSendHandler;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.TextChannel;
import request.AddRequest;

/**
 * Classe qui gère la musique et la file d'attente
 * @author hugo
 *
 */
public class MusicManager {
	
	private static Map<Guild, ServerData> map = new HashMap<Guild, ServerData>();
	private static AudioPlayerManager manager = new DefaultAudioPlayerManager();
	
	static {
		AudioSourceManagers.registerRemoteSources(manager);
	}
	
	/**
	 * Donnée stocké pour chaque serveur
	 * @author hugo
	 *
	 */
	private static class ServerData {
		public AudioPlayer player;
		public Queue<Member> memberQueue;
		public Map<Member, Queue<AudioTrack>> memberMap;
		public TextChannel defaultTextChannel;
		public Member actualMember;
	}
	
	/**
	 * Fonction pour ajouter une nouvelle guild dans le registre du bot
	 * @param guild La guilde
	 */
	public static void add(Guild guild) {
		if(!map.containsKey(guild)) {
			ServerData data = new ServerData();
			AudioPlayer player = manager.createPlayer();
			player.setVolume(50);
			player.addListener(new AudioEventAdapter() {
				
				private Guild guildOfTrack = guild;
				
				@Override
				public void onPlayerPause(AudioPlayer player) {
					
				}
				
				@Override
				public void onPlayerResume(AudioPlayer player) {
					
				}
				
				@Override
				public void onTrackStart(AudioPlayer player, AudioTrack track) {

				}
				
				@Override
				public void onTrackEnd(AudioPlayer player, AudioTrack track, AudioTrackEndReason endReason) {
					System.out.println("Oui j'ai été call");
					playNextMusic(guildOfTrack);
				}
				
				@Override
				public void onTrackException(AudioPlayer player, AudioTrack track, FriendlyException exception) {
				    map.get(guild).defaultTextChannel.sendMessage("Cette musique ne passe pas :/").complete();
				}
				
				@Override
				public void onTrackStuck(AudioPlayer player, AudioTrack track, long thresholdMs) {
					
				}
				
			});
			data.player = player;
			data.memberQueue = new ArrayDeque<Member>();
			data.memberMap = new HashMap<Member, Queue<AudioTrack>>();
			data.defaultTextChannel = null;
			map.put(guild, data);
		}
	}
	
	/**
	 * Fonction qui retourne l'audio player associé a une guilde
	 * @param guild La guilde
	 * @return L'audio player associé
	 */
	public static AudioPlayer getAudioPlayer(Guild guild) {
		return map.get(guild).player;
	}
	
	/**
	 * Fonction qui retourne la file d'attente des member associé a une Guilde
	 * @param guild La guilde
	 * @return La file d'attente associé
	 */
	public static Queue<Member> getQueue(Guild guild) {
		return map.get(guild).memberQueue;
	}
	
	/**
	 * Fonction qui retourne une map associé a chaque Membre d'une guilde les liens des musiques voulus
	 * @param guid La guilde
	 * @return La map
	 */
	public static Map<Member, Queue<AudioTrack>> getMemberMap(Guild guild){
		return map.get(guild).memberMap;
	}
	
	/**
	 * Retourne le channel textuel par default pour cette guild
	 * @param guild La guilde
	 * @return Le channel textuel par defualt
	 */
	public static TextChannel getTextChannel(Guild guild) {
		return map.get(guild).defaultTextChannel;
	}
	
	/**
	 * Change le channel textuel par default pour cette guild
	 * @param guild La guilde
	 * @param channel Le nouveau channel
	 */
	public static void setTextChannel(Guild guild, TextChannel channel) {
		map.get(guild).defaultTextChannel = channel;
	}
	
	public static Member getActualMember(Guild guild) {
		return map.get(guild).actualMember;
	}
	
	public static void setActualMember(Guild guild, Member member) {
		map.get(guild).actualMember = member;
	}
	
	/**
	 * Fonction traitant une requête pour ajouter une musique
	 * @param request La requête
	 */
	public static void addRequest(AddRequest request) {
		Guild guild = request.MEMBER.getGuild();
		
		if(!map.containsKey(guild)) {
			add(guild);
		}
		
		Queue<Member> queue = getQueue(guild);
		if(!queue.contains(request.MEMBER)) {
			queue.add(request.MEMBER);
			if(queue.size() == 2) queue.add(queue.poll());
		}
		
		Map<Member, Queue<AudioTrack>> memberMap = getMemberMap(guild);
		if(!memberMap.containsKey(request.MEMBER)) {
			memberMap.put(request.MEMBER, new LinkedList<AudioTrack>());
		}
		String identifier;
		if(request.isSearched) identifier = "ytsearch:" + request.keyWord;
		else identifier = request.keyWord;
		manager.loadItem(identifier, new AudioLoadResultHandler() {
			
			private MessageBuilder builder = new MessageBuilder();
			
			@Override
			public void trackLoaded(AudioTrack track) {
				memberMap.get(request.MEMBER).add(track);
				builder.append(track.getInfo().title + " a été ajouté a la file d'attente");
				sendMessage();
				startMusic(guild);
			}
			
			@Override
			public void playlistLoaded(AudioPlaylist playlist) {
				List<AudioTrack> tracks = playlist.getTracks();
				if(playlist.isSearchResult()) {
					trackLoaded(tracks.get(0));
				}
			}
			
		 	@Override
			public void noMatches() {
				builder.append("Je n'ai pas pu trouver cette musique snif snif");
				sendMessage();
			}
			
			@Override
			public void loadFailed(FriendlyException exception) {
				builder.append("Je n'ai pas pu charger cette musique snif snif");
				sendMessage();
			}
			
			private void sendMessage() {
				setTextChannel(guild, request.TEXT_CHANNEL);
				builder.sendTo(request.TEXT_CHANNEL).queue();
			}
		});
	}
	
	/**
	 * Fonction pour commencer le prochain morceau dans une guilde si possible
	 * @param guild La guilde
	 */
	private static void startMusic(Guild guild) {
		AudioTrack track = getAudioPlayer(guild).getPlayingTrack();
		if(track == null) playNextMusic(guild);
	}
	
	/**
	 * Fonction pour passer une musique
	 * @param guild La guilde
	 */
	public static void skipMusic(Guild guild) {
		getAudioPlayer(guild).stopTrack();
	}
	
	/**
	 * Fonction pour forcer a commencer le prochain morceau dans une guilde
	 * @param guild La guilde
	 */
	private static void playNextMusic(Guild guild) {
		if(!getAudioTrackQueue(guild).isEmpty()) {
			AudioPlayer player = getAudioPlayer(guild);
			do {
				Member newMember = getQueue(guild).poll();
				getQueue(guild).add(newMember);
				setActualMember(guild, newMember);
				player.playTrack(getMemberMap(guild).get(newMember).poll());
			}while(player.getPlayingTrack() == null);
			guild.getAudioManager().setSendingHandler(new AudioSendHandler() {
				
				private final AudioPlayer audioPlayer = player;
				private AudioFrame frame;
				
				@Override
				public byte[] provide20MsAudio() {
					return frame.getData();
				}
				
				@Override
				public boolean canProvide() {
					frame = audioPlayer.provide();
					return frame != null;
				}
				
				@Override
				public boolean isOpus() {
					return true;
				}
			});
			new MessageBuilder("Et maintenant, je lance : " + player.getPlayingTrack().getInfo().title).sendTo(getTextChannel(guild)).queue();
		}
	}
	
	/**
	 * Fonction pour forcer le bot a quitter le vocal
	 * @param guild La guilde
	 */
	public static void forceQuit(Guild guild) {
		guild.getAudioManager().closeAudioConnection();
		getQueue(guild).clear();
		getMemberMap(guild).clear();
		getAudioPlayer(guild).stopTrack();
	}
	
	/**
	 * Classe représentant une entrée de la queue
	 * @author hugo
	 *
	 */
	public static class QueueEntry{
		public final AudioTrack track;
		public final Member member;
		
		public QueueEntry(AudioTrack track, Member member) {
			super();
			this.track = track;
			this.member = member;
		}
	}
	
	/**
	 * Fonction qui retourne les prochaines musiques que le bot lancera
	 * @param guild La guilde
	 * @return Les prochaines musiques sous forme de queue
	 */
	public static Queue<QueueEntry> getAudioTrackQueue(Guild guild){
		Queue<QueueEntry> res = new LinkedList<QueueEntry>();
		Map<Member, Integer> nbMusicPutted = new HashMap<Member, Integer>();
		Queue<Member> orderMember = getQueue(guild);
		Map<Member, List<AudioTrack>> trackWanted = new HashMap<Member, List<AudioTrack>>();
		
		/* Initialisation nbmusicPutted et trackWanted */
		for(Entry<Member, Queue<AudioTrack>> entry : getMemberMap(guild).entrySet()) {
			nbMusicPutted.put(entry.getKey(), 0);
			trackWanted.put(entry.getKey(), new LinkedList<AudioTrack>(entry.getValue()));
		}
		
		boolean fini;
		do{
			fini = true;
			for(Member member : orderMember) {
				if(nbMusicPutted.get(member) < trackWanted.get(member).size()) {
					res.add(new QueueEntry(trackWanted.get(member).get(nbMusicPutted.get(member)), member));
					nbMusicPutted.put(member, nbMusicPutted.get(member) + 1);
					fini = false;	
				}
			}
		}while(!fini);
		return res;
	}

}
